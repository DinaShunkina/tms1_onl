from operator import attrgetter


class Flower:
    def __init__(self, time: int, color, name, length, price: int):
        self.time = time
        self.color = color
        self.name = name
        self.length = length
        self.price = price


class Bouquet:
    def __init__(self, name, flowers):
        self.name = name
        self.flowers_in_bouquet = list(flowers)

    def check_bouquet_price(self):
        """ Проверка стоимости букета """
        return sum([flower.price for flower in self.flowers_in_bouquet])

    def check_life_time(self):
        """ Проверка времени жизни букета """
        result = sum([flower.time for flower in self.flowers_in_bouquet])
        return result / len(self.flowers_in_bouquet)

    def sort_by_attr(self, attribute):
        """ Сортировка по атрибуту """
        return sorted(self.flowers_in_bouquet, key=attrgetter(attribute))

    def search_flower_in_bouquet(self, attribute: str, value):
        """ Поиск букета """
        for flower in self.flowers_in_bouquet:
            # создаю словарь со всеми и атрибутами и значениями атрибутов
            d = {attr: getattr(flower, attr) for attr in flower.__dict__}
            for k in d.keys():
                if k == attribute and d[k] == value:
                    return f"This flower is {flower.name}"

    def __contains__(self, flower):
        """ Проверяю есть ли цветок в букете """
        return flower in self.flowers_in_bouquet


flower_1 = Flower(time=2, color="red",
                  name="rose", length="12", price=8)
flower_3 = Flower(time=2, color="red", name="rose",
                  length="12", price=8)
flower_2 = Flower(time=3, color="yellow", name="tulip",
                  length="7", price=5)


first_bouquet = Bouquet(name="Beautiful",
                        flowers=[flower_1, flower_2, flower_3])
print(first_bouquet.check_bouquet_price())
print(first_bouquet.check_life_time())
print(first_bouquet.sort_by_attr(attribute="color"))
print(first_bouquet.search_flower_in_bouquet(attribute="color",
                                             value="yellow"))
print(first_bouquet.search_flower_in_bouquet(attribute="length",
                                             value="12"))
print(first_bouquet.search_flower_in_bouquet(attribute="color",
                                             value="red"))
print(flower_1 in first_bouquet)
