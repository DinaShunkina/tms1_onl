from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import pytest


@pytest.fixture(scope="class")
def browser_hm():
    page = webdriver.Chrome()
    page.implicitly_wait(4)
    page.get("http://the-internet.herokuapp.com/dynamic_controls")
    yield page
    page.quit()


class TestHmCheckBox:

    def test_first(self, browser_hm):
        remove_button = browser_hm\
            .find_element_by_xpath("//button[contains(., 'Remove')]")
        remove_button.click()
        assert WebDriverWait(browser_hm, 50).\
            until(EC.presence_of_element_located((By.ID, 'message')))\
            .is_displayed()

    def test_second(self, browser_hm):
        assert len(browser_hm.find_elements(By.ID, "checkbox")) == 0

    def test_third(self, browser_hm):
        xpath = "//form[@id='input-example']//input[@disabled]"
        assert browser_hm.find_element_by_xpath(xpath)

    def test_four(self, browser_hm):
        enable_button = browser_hm\
            .find_element_by_xpath("//button[contains(., 'Enable')]")
        enable_button.click()
        xpath = "//p[@id='message' and contains(., 'It's enabled!')]"
        assert WebDriverWait(browser_hm, 50)\
            .until(EC.presence_of_element_located((By.XPATH, xpath)))\
            .is_displayed()

    def test_five(self, browser_hm):
        xpath = "//form[@id='input-example']//input"
        assert browser_hm.find_element_by_xpath(xpath).is_enabled()
