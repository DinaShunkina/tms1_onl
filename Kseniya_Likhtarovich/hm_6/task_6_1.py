"""
Написать программу, принимающее число - номер кредитной карты.

Программа проверяет, может ли такая карта существовать.
Предусмотреть защиту от ввода букв, пустой строки и т.д.
"""


def validate(number):
    """Проверяет правильность ввода номера по алгоритму Луна."""
    rev_lst = [int(i) for i in number][::-1]
    odd_sm = sum(rev_lst[0::2])
    even_sm = sum([sum(divmod(j * 2, 10)) for j in rev_lst[1::2]])
    return (odd_sm + even_sm) % 10 == 0


def protect():
    """Проверяет ввод некорректных данных."""
    number_inp_tr = True
    while number_inp_tr:
        number_inp = input("Введите номер карты: ")
        if not number_inp.isdigit():
            print("Введите 16 цифр (без букв, пробелов и т.д.)")
        else:
            print(validate(number_inp))
            number_inp_tr = False


protect()
