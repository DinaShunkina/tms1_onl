"""
Напишите функцию, принимающую на вход одномерный массив и два числа.

На выходе - матрица нужного размера, сконструированная из элементов массива.
"""


def reshape(lst, n, m):
    """Преобразует массив в матрицу."""
    arr = [[] for _ in range(n)]
    cnv_lst = (el for el in lst)
    for i in arr:
        for j in cnv_lst:
            i.append(j)
            if len(i) >= m:
                break
    return arr


print(reshape([1, 2, 3, 4, 5, 6], 2, 3))
print(reshape([1, 2, 3, 4, 5, 6, 7, 8], 4, 2))
