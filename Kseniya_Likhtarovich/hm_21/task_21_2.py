"""
Работа с xml файлом.

Разработать поиск книги по автору(часть имени)/цене/заголовку/описанию.
"""

import xml.etree.ElementTree as ET


def parseXML(root, tag, text):
    """Поиск книги по параметрам."""
    for child in root:
        for element in child:
            if element.tag == tag and text in element.text:
                print(child.attrib["id"])


root = ET.parse("library.xml").getroot()

parseXML(root, "author", "Kim")
parseXML(root, "price", "36.75")
parseXML(root, "title", "Ascendant")
parseXML(root, "description", "the collapse of a nanotechnology")
