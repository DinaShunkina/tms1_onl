"""
Создайте программу, которая принимает массив имён.

Программа возвращает строку (описание количества лайков).
Работает на нескольких языках.
"""

from textblob import TextBlob


def likes(names):
    """Возвращает строку в зависимости от языка и количества имён."""
    if not names:
        return "no one likes this"

    lang = TextBlob(f"{names}").detect_language()

    if len(names) == 1:
        result1 = f"{names[0]} likes this"
        if lang == "en":
            return result1
        else:
            return TextBlob(result1).translate(to="ru")
    elif len(names) == 2:
        result2 = f"{names[0]} and {names[1]} like this"
        if lang == "en":
            return result2
        else:
            return TextBlob(result2).translate(to="ru")
    elif len(names) == 3:
        result3 = f"{names[0]}, {names[1]} and {names[2]} like this"
        if lang == "en":
            return result3
        else:
            return TextBlob(result3).translate(to="ru")
    elif len(names) >= 4:
        rslt4 = f"{names[0]}, {names[1]} and {len(names[2:])} others like this"
        if lang == "en":
            return rslt4
        else:
            return TextBlob(rslt4).translate(to="ru")


print(likes(input().split()))
