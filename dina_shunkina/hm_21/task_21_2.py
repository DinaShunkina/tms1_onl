"""работа с xml файлом
Разработайте поиск книги в библиотеке по ее
автору(часть имени)/цене/заголовку/описанию."""
import xml.etree.cElementTree as ET

root = ET.parse("library.xml").getroot()


def search_by_author(root, attribute: str, value: str):
    for child in root:
        for attrib_child in list(child):
            if attrib_child.tag == attribute and \
                    attrib_child.text.find(value) >= 0:
                print(f"{attrib_child.tag} of book - {attrib_child.text}")


def search_by_price(root, attribute: str, value: str):
    for child in root:
        for attrib_child in list(child):
            if attrib_child.tag == attribute and value == attrib_child.text:
                print(f"{attrib_child.tag} of book - {attrib_child.text}")


def search_by_title(root, attribute: str, value: str):
    for child in root:
        for attrib_child in list(child):
            if attrib_child.tag == attribute and attrib_child.text.find(value):
                print(f"{attrib_child.tag} of book - {attrib_child.text}")


def search_by_description(root, attribute: str, value: str):
    for child in root:
        for attrib_child in list(child):
            if attrib_child.tag == attribute and \
                    attrib_child.text.find(value) >= 0:
                print(f"{attrib_child.tag} of book - {attrib_child.text}")


search_by_author(root, "author", "Shaw")
search_by_price(root, "price", "44.95")
search_by_price(root, "title", "Midnight Rain")
search_by_description(root, "description", "An in-depth look ")
