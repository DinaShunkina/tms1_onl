import pytest
from selenium import webdriver


@pytest.fixture()
def browser():
    driver = webdriver.Chrome("./chromedriver")
    driver.maximize_window()
    driver.implicitly_wait(3)
    yield driver
    driver.quit()
