def typed(types):
    def decorator(func):
        def wrapper(*args):
            if types == "str":
                args = map(str, args)
            elif types == "int":
                args = map(int, args)
            elif types == "float":
                args = map(int, args)
            return func(*args)

        return wrapper

    return decorator


@typed(types="str")
def add_two_symbols(a, b):
    return a + b


print(add_two_symbols("3", 5))
print(add_two_symbols(5, 5))
print(add_two_symbols('a', 'b'))


@typed(types="int")
def add_three_symbols(a, b, c):
    return a + b + c


print(add_three_symbols(5, 6, 7))
print(add_three_symbols("3", 5, 0))


@typed(types="float")
def add_three_symbols(a, b, c):
    return a + b + c


print(add_three_symbols(0.1, 0.2, 0.4))
