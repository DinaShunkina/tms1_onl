class Investition:

    # where n is amount of money, r is a term in month
    # and we know that percent is 10%
    def __init__(self, n, r):
        self.n = n
        self.r = r


class Bank:
    def __init__(self, name):
        self.name = name

    def deposit(self, investition):

        # Formula is taken from here https://take.ms/NlxuE
        result = investition.n * (1 + 0.1 / 12) ** investition.r
        print(f"After {investition.r} month you receive {round (result, 2)}")
        return result


# Test how it works
test_invest = Investition(300, 12)
bank_1 = Bank("OTP")

bank_1.deposit(test_invest)
