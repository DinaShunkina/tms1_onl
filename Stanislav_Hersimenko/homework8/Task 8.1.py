def decorator(typed):
    def wrapper_first(func):
        def wrapper_second(*args):
            if typed == "str":
                args = map(str, args)
            elif typed == "float":
                args = map(float, args)
            return func(*args)
        return wrapper_second
    return wrapper_first


@decorator(typed="str")
def add(a, b):
    return a + b


print(add("3", 5))
print(add(5, 5))
print(add("a", "b"))


@decorator(typed="float")
def add(a, b, c):
    return a + b + c


print(add(5, 6, 7))
print(add(0.1, 0.2, 0.4))


@decorator(typed="float")
def add(a, b):
    return a + b


print(add("3", 5))
