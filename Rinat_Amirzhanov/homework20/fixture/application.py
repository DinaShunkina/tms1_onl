import allure
from allure_commons.types import AttachmentType
from selenium import webdriver

from Rinat_Amirzhanov.homework20.pages.page_for_test import TestPageHelper


class Application:

    def __init__(self):
        driver = webdriver.Chrome('./chromedriver.exe')

        # Это капабилити селеноида, который использую
        # для запуска браузера на своем проекте
        # capabilities = {
        #     "browserName": "chrome",
        #     "browserVersion": "90",
        #     "selenoid:options": {
        #         "enableVNC": True,
        #         "enableVideo": False
        #     }
        # }
        #
        # driver = webdriver.Remote(
        #     command_executor="http://localhost:4444/wd/hub",
        #     desired_capabilities=capabilities)
        #
        # self.driver = driver
        # self.driver.implicitly_wait(120)
        # self.driver.maximize_window()
        # self.home_page = HomePageHelper(self)

        self.driver = driver
        self.driver.maximize_window()
        self.test_page = TestPageHelper(self)

    def destroy(self):
        allure.attach(self.driver.get_screenshot_as_png(),
                      attachment_type=AttachmentType.PNG)
        self.driver.quit()
