a_ru = ord('а')
alphabet_ru = ''.join(
    [chr(i) for i in
     range(a_ru, a_ru + 6)] + [chr(a_ru + 33)] + [chr(i) for i in
                                                  range(a_ru + 6, a_ru + 32)])
alphabet_ru = alphabet_ru + alphabet_ru
a_en = ord('a')
alphabet_en = ''.join([chr(i) for i in range(a_en, a_en + 26)])
alphabet_en = alphabet_en + alphabet_en


def caesar_algorithm_encode(word, step, lang):
    result = ""
    if lang == "en" or "ru":
        for i in word:
            if i in alphabet_en:
                position = alphabet_en.find(i)
                another_position = position + step
                result = result + alphabet_en[another_position]
            elif i in alphabet_ru:
                position = alphabet_ru.find(i)
                another_position = position + step
                result = result + alphabet_ru[another_position]
            else:
                result = result + i
        return result


def caesar_algorithm_decode(word, step, lang):
    result = ""
    if lang == "en" or "ru":
        for i in word:
            if i in alphabet_en:
                position = alphabet_en.find(i)
                another_position = position - step
                result += alphabet_en[another_position]
            elif i in alphabet_ru:
                position = alphabet_ru.find(i)
                another_position = position - step
                result += alphabet_ru[another_position]
            else:
                result += i
        return result


def check_result(word, step, lang):
    encode_result = caesar_algorithm_encode(word, step, lang)
    print(f"Word: {word}\nEncode: {encode_result}\n"
          f"Decode: {caesar_algorithm_decode(encode_result, step, lang)}\n")


check_result("hello world!", 3, "en")
check_result("this is a test string", 3, "en")
check_result("строка", 2, "ru")
