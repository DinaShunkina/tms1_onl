import unittest
from unittest import expectedFailure
from program import game_bulls


class TestBulls(unittest.TestCase):

    def test_bulls_3(self):
        actual = game_bulls(9743, 9742)
        expected = [3, 0]
        self.assertEqual(actual, expected)

    def test_bulls_1(self):
        actual = game_bulls(5129, 5034)
        expected = [1, 0]
        self.assertEqual(actual, expected)

    def test_cows_4(self):
        actual = game_bulls(4569, 9654)
        expected = [0, 4]
        self.assertEqual(actual, expected)

    def test_cows_1(self):
        actual = game_bulls(4569, 3004)
        expected = [0, 1]
        self.assertEqual(actual, expected)

    def test_total_failure(self):
        actual = game_bulls(4569, 1111)
        expected = [0, 0]
        self.assertEqual(actual, expected)

    def test_win(self):
        actual = game_bulls(4569, 4569)
        expected = "Вы выйграли!"
        self.assertEqual(actual, expected)

    @expectedFailure
    def test_empty(self):
        actual = game_bulls(4569, "")
        self.fail(actual)
