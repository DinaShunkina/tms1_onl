from selenium.webdriver.common.by import By


class LoginPageLocators:

    EMAIL_FIELD = (By.ID, "email")
    PASSWORD_FIELD = (By.ID, "passwd")
    SIGN_IN_BUTTON = (By.ID, "SubmitLogin")
    LOCATOR_ACCOUNT = (By.CLASS_NAME, "navigation_page")
    LOCATOR_ERROR_LOGIN = (
        By.XPATH, "//div[@class='alert alert-danger']/ol/li")
