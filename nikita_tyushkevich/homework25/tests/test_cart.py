from nikita_tyushkevich.homework25.pages.main_page import MainPage
from nikita_tyushkevich.homework25.pages.basket_page import BasketPage


def test_cart(driver):
    main_page = MainPage(driver)
    main_page.open_main_page()
    main_page.is_main_page()
    main_page.go_to_cart()
    basket = BasketPage(driver)
    basket.is_basket_page()
    basket.empty_basket()
