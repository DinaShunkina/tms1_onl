# task_7 Самая частая буква
from collections import Counter

str1 = "a-z"
str2 = str1.lower()
list1 = list(str2)
new_dict = Counter(list1)
new_list = []
for k in new_dict.keys():
    if new_dict[k] == max(new_dict.values()) and k.isalpha():
        new_list.append(k)
    elif k.isspace():
        continue
    elif k.isnumeric():
        continue
    else:
        continue
sorted_list = sorted(new_list)
print(sorted_list[0])

# "a-z" == "a"
# "Hello World!" == "l"
# "How do you do?" == "o"
# "One" == "e"
# "Oops!" == "o"
# "a" * 9000 + "b" * 1000 == "a"


# Case with "AAaooo!!!!" == "a"
# from collections import Counter is used there too

str1 = "AAaooo!!!!"
str2 = str1.lower()
list1 = list(str2)
new_dict = Counter(list1)
new_list = []
for k in new_dict.keys():
    if new_dict[k] == max(new_dict.values()) and not k.isalpha():
        continue
    elif k.isspace():
        continue
    elif k.isnumeric():
        continue
    else:
        new_list.append(k)
sorted_list = sorted(new_list)
print(sorted_list[0])
