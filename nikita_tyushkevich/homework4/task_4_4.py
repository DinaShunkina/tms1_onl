# Изменить список из 10 элементов
list1 = ["Sun", "Mercury", "Venus", "Earth", "Mars", "Jupiter",
         "Saturn", "Uranus", "Neptune", "Pluto"]
print(list1)
print("\n")

# Заменить значение на позиции 3
list1[2] = 3
print(list1)
print("\n")

# Удалить элемент с индексом 6
del list1[6]
print(list1)
