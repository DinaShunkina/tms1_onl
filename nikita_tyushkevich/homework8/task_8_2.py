# Lexicographic ascending
def dict_numbers(func):
    def wrapper(*args):
        list_values = []
        list_keys = []
        number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
                        5: 'five', 6: 'six', 7: 'seven', 8: 'eight',
                        9: 'nine', 10: 'ten', 11: 'eleven', 12: 'twelve',
                        13: 'thirteen', 14: 'fourteen', 15: 'fifteen',
                        16: 'sixteen', 17: 'seventeen', 18: 'eighteen',
                        19: 'nineteen'}
        # Numbers names getting
        for i in args:
            for k, v in number_names.items():
                if i == k:
                    list_values.append(v)
        # Numbers names sorting
        list_values.sort()
        # Getting numbers in correct order
        for s in list_values:
            for k, v in number_names.items():
                if s == v:
                    list_keys.append(k)
        func(list_keys)
    return wrapper


@dict_numbers
def received_numbers(numbers):
    print(numbers)


received_numbers(1, 2, 3, 4, 11, 12, 15, 19)
