import pytest
from symbols_count import letters_count


@pytest.mark.parametrize('param, ret_value', [('aaaa', 'a4'),
                                              ('cccbba', 'c3b2a'),
                                              ('abeehhhhccced', 'abe2h4c3ed'),
                                              ('abeehhccceda', 'abe2h2c3eda'),
                                              ('!!!$#@%%%%', '!3$#@%4'),
                                              ('55517799002', '5317292022'),
                                              ('拉进群', '拉进群')])
class TestMainPositive:

    @pytest.mark.positive
    def test_same_letter(self, param, ret_value):
        assert letters_count(param) == ret_value, f"Should be {ret_value}"


@pytest.mark.parametrize('param, ret_value', [(55517, '5317'),
                                              (555.17, '53.17'),
                                              (555 + 18, '53 + 18'),
                                              (True, 'True'),
                                              ([2, "num"], '[2, "num"]')
                                              ])
class TestMainNegative:

    @pytest.mark.negative
    @pytest.mark.xfail(reason="Expected fail")
    def test_int(self, param, ret_value):
        assert param == ret_value, f"Should be {ret_value}"
