from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

browser = webdriver.Chrome('./chromedriver.exe')
browser.implicitly_wait(5)
browser.get("http://the-internet.herokuapp.com/frames")


def test_iframe():
    link = browser.find_element(By.XPATH, '//a[@href="/iframe"]')
    link.click()
    WebDriverWait(browser, 3).until(
        EC.frame_to_be_available_and_switch_to_it((By.XPATH, "//iframe")))
    message = browser.find_element_by_id('tinymce')
    assert message.text == "Your content goes here."
    browser.quit()
