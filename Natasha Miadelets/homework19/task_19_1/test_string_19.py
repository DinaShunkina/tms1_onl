from main_string import count_letters
import pytest


number_1 = "aacccchhhhheebbbc"
number_2 = "aaajjjffhfdjkj"
number_3 = "ffeeerrrree"
answer_1 = "a2c4h5e2b3c"
answer_2 = "a3j3f2hfdjkj"
answer_3 = "f2e3r4e2"
answer_wrong = "f2e5r4"


@pytest.fixture(scope="session")
def start_test():
    print("Start test")


@pytest.mark.daily
def test_1(start_test):
    assert count_letters(number_1) == answer_1


@pytest.mark.daily
@pytest.mark.parametrize('kind', ["A", "B"])
def test_2(start_test, kind):
    assert count_letters(number_2) == answer_2


@pytest.mark.daily
def test_3(start_test):
    assert count_letters(number_3) == answer_3


@pytest.mark.xfail(reason="bug 189")
def test_wrong_answer(start_test):
    assert count_letters(number_3) == answer_wrong
