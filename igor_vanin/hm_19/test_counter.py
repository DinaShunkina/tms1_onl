import pytest
from counting_characters_main import counter


@pytest.fixture(params=["ab2c3"])
def smoke_symb(request):
    return request.param


@pytest.mark.smoke
class TestSmokeCases:
    def test_smoke_counter(self, smoke_symb):
        assert counter('abbccc') == smoke_symb

    def test_cyrillic(self):
        assert counter("абб") == 'аб2'

    def test_one_symbols(self):
        assert counter('a') == 'a'


@pytest.mark.regression
class TestRegressionCases:
    def test_smoke_counter(self):
        assert counter("aaaaa") == "a5"

    def test_cyrillic(self):
        assert counter(1223334444) == "1223344"

    def test_one_symbols(self):
        assert counter("@#$%^&") == "@#$%^&"

    def test_float_symbols(self):
        assert counter(1.1) == "1.1"


class TestNegativeCasesAndBugs:
    # IndexError validation
    def test_null_symbols(self):
        with pytest.raises(IndexError):
            assert counter("")

    @pytest.mark.xfail(reason="bug trim space")
    def test_trim_space(self):
        assert counter("aa  ") == "a2"
