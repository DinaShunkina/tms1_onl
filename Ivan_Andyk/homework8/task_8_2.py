number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
                5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine',
                10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen',
                14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
                17: 'seventeen', 18: 'eighteen', 19: 'nineteen'}


def decorator(func):
    def wrapper(num: str):
        word = []
        for i in num:
            for k, v in number_names.items():
                if i == str(k):
                    word.append(v)
        word.sort()
        new_order = []
        for a in word:
            for k, v in number_names.items():
                if a == v:
                    new_order.append(k)
        func(new_order)
    return wrapper


@decorator
def sorted_numbers(numb):
    print(numb)


numbers = input("Введите числа от 0 до 19: ")
sorted_numbers(numbers)
