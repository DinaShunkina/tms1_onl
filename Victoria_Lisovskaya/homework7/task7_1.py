def reshape(a: list, b: int, c: int):
    if b * c <= len(a):
        x = []
        for i in range(b):
            z = [el for el in a[i * c:i * c + c]]
            x.append(z)
        print(x)
    else:
        print("Incorrect input data")


reshape([1, 2, 3, 4, 5, 6], 2, 3)
