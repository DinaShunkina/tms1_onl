number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three',
                4: 'four', 5: 'five', 6: 'six', 7: 'seven',
                8: 'eight', 9: 'nine', 10: 'ten', 11: 'eleven',
                12: 'twelve', 13: 'thirteen', 14: 'fourteen',
                15: 'fifteen', 16: 'sixteen', 17: 'seventeen',
                18: 'eighteen', 19: 'nineteen'}


def decorator_number(func):
    def wrapper(li):
        number_names_index = {}
        numbers = li.split(" ")
        for key in numbers:
            number_names_index[int(key)] = number_names[int(key)]
        list_d = sorted(number_names_index.items(), key=lambda a: a[1])
        for i in list_d:
            func(print(i[0], end=' '))

    return wrapper


@decorator_number
def number(*args):
    return args


number('1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19')
