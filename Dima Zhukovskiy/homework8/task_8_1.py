def typed(types):
    def decorator(func):
        def wrapper(*args):
            if types == 'str':
                args = map(str, args)
            if types == 'int':
                args = map(int, args)
            if types == 'float':
                args = map(float, args)
            return func(*args)
        return wrapper
    return decorator


@typed(types='str')
def add(a, b):
    return a + b


print(add("3", 5))
print(add(5, 5))
print(add('a', 'b'))


@typed(types='int')
def add(a, b, c):
    return a + b + c


print(add(5, 6, 7))
print(add("3", 5, 4))


@typed(types='float')
def add(a, b, c):
    return a + b + c


print(add(0.1, 0.2, 0.4))
